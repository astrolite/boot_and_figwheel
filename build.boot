(clojure.java.io/make-parents "resources/public/.empty")

(set-env! :source-paths #{"src"}
          :resource-paths #{"resources"}
          :dependencies '[[adzerk/boot-cljs "1.7.228-1" :scope "test"]
                          [org.clojure/clojurescript "1.7.228"]
                          [org.clojure/clojure "1.8.0"]
                          [http-kit "2.2.0"]
                          [pandeiro/boot-http "0.7.3" :scope "test"]
                          [ajchemist/boot-figwheel "0.5.4-6" :scope "test"]
                          [figwheel-sidecar "0.5.7" :scope "test"]
                          [org.clojure/tools.nrepl "0.2.12" :scope "test"]
                          [com.cemerick/piggieback "0.2.1" :scope "test"]
                          [nightlight "1.6.3" :scope "test"]
                          [bidi "2.0.14"]])

(require
 '[adzerk.boot-cljs :refer [cljs]]
 '[pandeiro.boot-http :refer [serve]]
 '[boot-figwheel :refer [figwheel]]
 '[nightlight.boot :refer [nightlight]])

(deftask prod_cljs []
   (figwheel
    :build-ids ["a"]
    :all-builds [{:id "a"
                  :source-paths ["src"]
                  :compiler '{:main          a.client
                              :output-to     "a.js"
                              :output-dir    "a"
                              :optimizations  :advanced}
                  :figwheel false}]
    :target-path "resources/public"))

(deftask dev []
  (comp
   (figwheel
    :build-ids ["a"]
    :all-builds [{:id "a"
                  :source-paths ["src"]
                  :compiler '{:main          a.client
                              :output-to     "a.js"
                              :output-dir    "a"
                              :optimizations  :none
                              :source-map     true}
                  :figwheel true}]
    :figwheel-options {:server-port 7000
                       :server-logfile ".figwheel_server.log"
                       :validate-config false}
    :target-path "resources/public")
   (serve :handler 'a.server/app
          :reload true
          :port 8080
          :httpkit true)
   (repl)
   (nightlight :port 4000)))

#_
(task-options!
 jar    {:main 'foo.bar}
 target {:dir #{"target"}}
 sift   {:include #{#"\.jar$"}}
 aot    {:namespace #{'foo.bar}}
 pom    {:project 'foo/bar :version "0.1.0-SNAPSHOT"})

(deftask uberjar []
  (comp (uber)
        (aot :namespace #{'a.server})
        (pom :project 'a :version "0.1.0")
        (jar :main 'a.server)
        (sift :include #{#"\.jar$" #"public/a\.js$"})
        (target :dir #{"target"})))

;; https://github.com/boot-clj/boot/wiki/Boot-for-Leiningen-Users
(deftask prod []
  (with-pass-thru _
    (prn :start-prod_cljs)
    (boot (prod_cljs))
    (prn :start-uberjar)
    (boot (uberjar))))
