(ns a.client)

(defn main []
  (js/alert 123))

(when (not (aget js/window "main"))
  (main)
  (aset js/window "main" 1))

;;(js/alert 44)
