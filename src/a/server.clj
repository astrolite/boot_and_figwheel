(ns a.server
  (:gen-class)
  (:require org.httpkit.server
            [ring.middleware.resource :refer [wrap-resource]]
            [bidi.ring :refer [make-handler]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]))

(def app
  (->
   (make-handler
    ["/" (fn [r] {:status 200 :body "<script src=\"a.js\"></script>"
                  :headers {"Content-Type" "text/html; charset=UTF-8"}})])
   (wrap-resource "public")
   wrap-not-modified))

(defn -main []
  (org.httpkit.server/run-server app {:port 8080}))

